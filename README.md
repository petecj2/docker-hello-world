docker-hello-world
==================

Sample docker image to test docker deployments

Inspired by https://github.com/tutumcloud/hello-world
Builds upon https://hub.docker.com/_/php/


Usage
-----

To create the image `petecj2/docker-hello-world`, execute the following command on the docker-hello-world folder:

	docker build -t petecj2/docker-hello-world .

You can now push your new image to the registry:

	sudo docker push petecj2/docker-hello-world


Running your Hello World docker image
-------------------------------------

Start your image:

	sudo docker run -d -p 80 petecj2/docker-hello-world

It will print the new container ID (like `d35bf1374e88`). Get the allocated external port:

	sudo docker port d35bf1374e88 80

It will print the allocated port (like 4751). Test your deployment:

	curl http://localhost:4751/


Hello world!

**by petecj2**
